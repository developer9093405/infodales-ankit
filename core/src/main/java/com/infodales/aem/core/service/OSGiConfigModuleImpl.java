package com.infodales.aem.core.service;

import com.infodales.aem.core.config.ankitOSGiConfig;
import com.infodales.aem.core.service.impl.OSGiConfigModule;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;

@Component(service = OSGiConfigModule.class ,immediate = true)
@Designate(ocd = ankitOSGiConfig.class)
public class OSGiConfigModuleImpl implements OSGiConfigModule{
    private String hostName;
    private int portNo;
    private String hostUniqueID;

    @Override
    public String getHostName() {
        return hostName;
    }

    @Override
    public int getPortNo() {
        return portNo;
    }

    @Override
    public String getHostUniqueID() {
        return hostUniqueID;
    }

    @Activate
    protected void activate(ankitOSGiConfig ankitOSGiConfig){
        hostName = ankitOSGiConfig.hostName();
        portNo = ankitOSGiConfig.portNo();
        hostUniqueID = ankitOSGiConfig.hostUniqueID();
    }



}
