package com.infodales.aem.core.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=/bin/Infodales/Profile"
        })
public class GetProfileServlet extends SlingSafeMethodsServlet {
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse resp) throws IOException {
        String value = "";
        HttpSession session = request.getSession();
        if (session != null) {
            Object sessionObject = session.getAttribute("user");
            if (sessionObject != null) {
                value = sessionObject.toString();
            } else {
                value = "session is empty";
            }
        } else {
            value = "Session is not creates session";
        }
        resp.getWriter().write(value);
    }
}