package com.infodales.aem.core.models;
import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = NewHeaderAnkitModel.class)
public class NewHeaderAnkitModel{
    @Inject
    private String link;
    @Inject
    private String fileReference;
    @Inject
    private String altText;
    @Inject
    private List<NavbarElementModel> myMultifield;

    @Inject
    private ResourceResolver resolver;

    @Inject
    private List<String> navLink;

    private List<NavLink> navLinkList;

    public String getLink() {
        return link;
    }
    public String getFileReference() {
        return fileReference;

    }
    public String getAltText() {
        return altText;
    }

    public List<NavbarElementModel> getMyMultifield() {
        return myMultifield;
    }

    public List<NavLink> getNavLinkList() {
        return navLinkList;
    }

    @PostConstruct
    protected void init(){
        if(navLink!=null){
            navLinkList = new ArrayList<NavLink>();
            for(String link : navLink){
                NavLink navLink = new NavLink();
                Resource resource = resolver.getResource(link);
                Page page = resource.adaptTo(Page.class);
                navLink.setLink(link);
                navLink.setPageTitle(page.getTitle());
                navLinkList.add(navLink);
            }
        }

    }
}
