package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Unpublished Asset Reference Mail"})
public class EmailUnpublishedAssetsStep implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Reference
    private EmailService emailService;

    @Reference
    private WorkflowNotificationService workflowNotificationService;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Unpublished Asset Reference Mail {}", payload);
        String initiator = workItem.getWorkflow().getInitiator();
        MetaDataMap wfMetaData = workItem.getWorkflowData().getMetaDataMap();
        boolean mailEnable = false;
        if (wfMetaData.containsKey(TCBConstants.EMAIL_ENABLE)) {
            mailEnable = wfMetaData.get(TCBConstants.EMAIL_ENABLE, Boolean.class);
        }

        String workflowTitle = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.WORKFLOW_TITLE)) {
            workflowTitle = wfMetaData.get(TCBConstants.WORKFLOW_TITLE, String.class);
        }

        String emailTitle = "Unpublished Page List";
        String emailBody = TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle + "Please publish the referenced asset.";


        ResourceResolver resolver = workflowSession.adaptTo(ResourceResolver.class);

        if (StringUtils.isNotBlank(initiator)) {
            workflowNotificationService.inboxNotification(resolver, initiator, emailTitle, emailBody, payload);
        }

        String email = wfMetaData.get(TCBConstants.USER_EMAIL, String.class);
        if (mailEnable && StringUtils.isNotBlank(email)) {
            String[] assetPathArr = wfMetaData.get(TCBConstants.ASSET_REFERENCE, String[].class);
            List<String> assetPathList = Arrays.asList(assetPathArr);
            String[] recipients = {email};
            StringBuilder sb = new StringBuilder();
            sb.append("<p>The below is the unpublished list of asset in page :</p>"
                    + payload);
            sb.append("<ul>");
            for (String assetPath : assetPathList) {
                sb.append("<li>" + assetPath + "</li>");
            }
            sb.append("</ul>");
            sb.append("<p>Please get the assets published and restart the page approval workflow again.</p>");
            emailService.sendEmail("Unpublished Page List", sb.toString(), recipients);
            logger.debug("Unpublished Asset Email Sent Successfully");
        }
    }
}
