package com.infodales.aem.core.servlets;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import javax.servlet.Servlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                 "sling.servlet.paths=/bin/Infodales/setsession"
        })
public class SetSessionServlet extends SlingAllMethodsServlet {
        private static final long serialVersionUID = 1L;

        @Override
        protected void doGet(final SlingHttpServletRequest request,
                              final SlingHttpServletResponse resp) throws IOException
        {
          String value=request.getParameter("user");

                HttpSession session=request.getSession();
                session.setAttribute("user", value);
                resp.getWriter().write("LogIn  successfull");
        }

}
