package com.infodales.aem.core.models;
import com.adobe.cq.dam.cfm.ContentFragment;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = UpcomingEvModel.class)
public class UpcomingEvModel {
    @Inject
    List<String> folderLink;

    @Inject
    ResourceResolver resolver;

    String yearFolderName;
    public String getyearFolderName() {
        return yearFolderName;
    }

    public void setyearFolderName(String yearFolderName) {
        this.yearFolderName = yearFolderName;
    }

    Map<String, List<UpcomingEvPojo>> upcomingEvPojoMap;

    @PostConstruct
    protected void init() {

        if (folderLink != null) {
            upcomingEvPojoMap = new HashMap<String, List<UpcomingEvPojo>>();
            for (String link : folderLink) {
                Resource resource = resolver.getResource(link);
                yearFolderName = resource.getName().toString();
                if(null != resource){
                    Iterator<Resource> childResources = resource.listChildren();
                    List<UpcomingEvPojo> upcoming = new ArrayList<UpcomingEvPojo>();
                    if(null != childResources){
                        while (childResources.hasNext()) {
                            Resource upcomingEventFargmentResource = childResources.next();
                            if (!"jcr:content".equals(upcomingEventFargmentResource.getName())) {
                                UpcomingEvPojo upcomingEventPojo = new UpcomingEvPojo();
//                                logger.info("Resource " + upcomingEventFargmentResource);
                                ContentFragment upcomingEventFargment = upcomingEventFargmentResource
                                        .adaptTo(ContentFragment.class);
                                upcomingEventPojo.setCfTitle(upcomingEventFargment.getTitle());
                                upcomingEventPojo.setPublishDate(upcomingEventFargment.getElement("publishDate").getContent().toString());
                                upcomingEventPojo.setExpiryDate(upcomingEventFargment.getElement("expiryDate").getContent().toString());
                                upcomingEventPojo.setImagePath(upcomingEventFargment.getElement("imagePath").getContent());
                                upcomingEventPojo.setQuota(upcomingEventFargment.getElement("quota").getContent());
                                upcomingEventPojo.setDestination(upcomingEventFargment.getElement("destination").getContent());
                                upcomingEventPojo.setOrganizer(upcomingEventFargment.getElement("organizer").getContent());
                                upcomingEventPojo.setIntroduction(upcomingEventFargment.getElement("introduction").getContent());
                                upcomingEventPojo.setFee(upcomingEventFargment.getElement("fee").getContent());
                                upcomingEventPojo.setSchedule(upcomingEventFargment.getElement("schedule").getContent());
                                upcomingEventPojo.setPrecautions(upcomingEventFargment.getElement("precautions").getContent());
                                upcomingEventPojo.setRegistrationStartDate(upcomingEventFargment.getElement("registrationStartDate").getContent().toString());
                                upcomingEventPojo.setRegistrationEndDate(upcomingEventFargment.getElement("deadlineForApplications").getContent().toString());
                                upcomingEventPojo.setRegistrationPageLink(upcomingEventFargment.getElement("registrationPageLink").getContent());
                                upcoming.add(upcomingEventPojo);

                            }
                            upcomingEvPojoMap.put(yearFolderName, upcoming);
                        }

                    }


                   
                }
               
            }
        }
    }

    public Map<String, List<UpcomingEvPojo>> getupcomingEvPojoMap() {
        return upcomingEvPojoMap;
    }
}
