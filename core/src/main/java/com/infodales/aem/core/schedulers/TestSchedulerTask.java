package com.infodales.aem.core.schedulers;

import com.day.cq.commons.jcr.JcrConstants;
import com.infodales.aem.core.config.testScheduledTaskConfig;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.Map;

import static com.infodales.aem.core.util.GetSystemUserResolver.PROJECT_SERVICE_USER;

@Component(service = Runnable.class,immediate = true)
@Designate(ocd = testScheduledTaskConfig.class)
public class TestSchedulerTask implements Runnable{
            @Reference
            ResourceResolverFactory resolverFactory;
    Logger log = LoggerFactory.getLogger(TestSchedulerTask.class);


    private int schedulerId;

    @Reference
    private Scheduler scheduler;


    @Deactivate
    protected void deactivate(testScheduledTaskConfig config){
        removeScheduler();
    }
private void removeScheduler(){
        scheduler.unschedule(String.valueOf(schedulerId));
}
private void addScheduler(testScheduledTaskConfig config){
    ScheduleOptions schedulerOptions = scheduler.EXPR(config.getCronExpression());
    schedulerOptions.name(String.valueOf(schedulerId));
    schedulerOptions.canRunConcurrently(false);
    scheduler.schedule(this, schedulerOptions);
    ScheduleOptions schedulerOptionsNow = scheduler.NOW(100,1);
    scheduler.schedule(this, schedulerOptionsNow);
    log.info("scheduler",scheduler);
log.info("\n-------------------------------Scheduler added successfully-------------------------------------");

}

//    protected void init() throws LoginException, RepositoryException {
//        final Map<String, Object> parameterMap = new HashMap<String, Object>();
//        parameterMap.put(ResourceResolverFactory.SUBSERVICE,PROJECT_SERVICE_USER);
//        ResourceResolver resolver = resolverFactory.getResourceResolver(parameterMap);
//        String parentPath = "/content/dam/infodales-ankit";
//        Resource infodalesResource = resolver.getResource(parentPath);
//        if (infodalesResource != null) {
//            Node parentNode = infodalesResource.adaptTo(Node.class);
//            try {
//                if (parentNode != null && !parentNode.hasNode("Child")) {
//
//                    Node newNode = parentNode.addNode("Child", JcrConstants.NT_UNSTRUCTURED);
//                    newNode.setProperty("name", "testnode");
//                    newNode.setProperty("men", true);
//                    newNode.setProperty("phoneno", 960775352);
//                    newNode.setProperty("bhai","yesBoos");
//                    String[] propertyValue = {"ankit", "aman", "Ram", "Krishna"};
//                    newNode.setProperty("studentnames", propertyValue);
//                    newNode.getSession().save();
//                }
//            } catch (RepositoryException e ) {
//
//            }
//
//        }
//    }

    /**
     *
     *
     *
     */
    @Override
    public void run() {
        log.info("\n==============Hi I have good news! for you ============");
        log.info("\n==============I Am Working Properly  ============");
        log.info("\n==============OK Bye ============");
    }

}
