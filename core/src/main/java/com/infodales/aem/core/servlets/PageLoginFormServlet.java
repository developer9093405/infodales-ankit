


package com.infodales.aem.core.servlets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = {Servlet.class},
property = {
        "sling.servlet.methods=" + HttpConstants.METHOD_POST,
        "sling.servlet.resourceTypes=" + "infodales-ankit/components/content/PageLoginForm",
        "sling.servlet.selectors=" + "value",
       "sling.servlet.extensions"+"=" + "json"
}
)
public class PageLoginFormServlet extends SlingAllMethodsServlet {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(PageLoginFormServlet.class);
    private static final long serialVersionUID = 1L;
@Override
protected void doPost(final SlingHttpServletRequest request,
                            final SlingHttpServletResponse response) throws IOException {
    String username = request.getParameter("username");
    logger.info("username:-",username);
    String password = request.getParameter("password");
    logger.info("Password",password);
    response.getWriter().write(username);
    response.getWriter().write(password);
}
}
