package com.infodales.aem.core.models;

import com.infodales.aem.core.service.impl.AnkitOSGiConfigCall;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = AnkitOSGiModel.class)
public class AnkitOSGiModel {

    @OSGiService
    AnkitOSGiConfigCall call;


    String hostName;
    int port;
    String hostUniqueId;

    public String getHostName() {
        return hostName;
    }

    public int getPort() {
        return port;
    }

    public String getHostUniqueId() {
        return hostUniqueId;
    }

    @PostConstruct
    protected void init(){
        hostName=call.getHostName();
        port=call.getPort();
        hostUniqueId=call.getUniqueId();


    }
}
