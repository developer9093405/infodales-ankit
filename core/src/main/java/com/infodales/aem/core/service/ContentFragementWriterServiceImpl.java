package com.infodales.aem.core.service;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentFragmentException;
import com.adobe.cq.dam.cfm.FragmentTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.infodales.aem.core.pojo.Branches;

import com.infodales.aem.core.service.impl.ContentFragementWriterService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONArray;
import org.osgi.service.component.annotations.Component;

import java.io.IOException;
import java.util.*;

@Component(service = ContentFragementWriterService.class,immediate = true)
public class ContentFragementWriterServiceImpl implements ContentFragementWriterService {
    @Override
    public void cfWriter(CloseableHttpResponse response, SlingHttpServletRequest req) throws IOException, ContentFragmentException {
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            String responseJson = EntityUtils.toString(entity);
            Gson gson = new Gson();
            Branches[] arrBranches = gson.fromJson(responseJson, Branches[].class);
            List<Branches> branchesList = new ArrayList<>();
            Collections.addAll(branchesList,arrBranches);

            Resource resource1 = req.getResource();
            ResourceResolver resourceResolver = resource1.getResourceResolver();
            Resource cfModel = resourceResolver.getResource("/conf/ContentFragmentWriter/settings/dam/cfm/models/branches");

            Resource parentRes = resourceResolver.getResource("/content/dam/contentfragmentwriterfolder/branches");
            FragmentTemplate template = cfModel.adaptTo(FragmentTemplate.class);

            for (Branches branches1 : branchesList) {
                ContentFragment branchesFragment = template.createFragment(parentRes, branches1.getName(), branches1.getName());
                resourceResolver.commit();
                ObjectMapper objectMapper = new ObjectMapper();
                Map<String,String> mapTest=objectMapper.convertValue(branches1, Map.class);

                Iterator<ContentElement> iter = branchesFragment.getElements();
                while (iter.hasNext()) {
                    ContentElement childCF = iter.next();
                    if (StringUtils.isNotBlank(childCF.getName())) {
                        childCF.setContent(mapTest.get(childCF.getName()), mapTest.get(childCF.getName()));
                    }
                }
                resourceResolver.commit();
            }
        }
    }

    @Override
    public CloseableHttpResponse hittingApi(String parameter) throws IOException {
        HttpGet request = new HttpGet("https://techcombank.com/api/data/"+parameter);
        request.addHeader("accept", "application/json");
        request.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 Firefox/26.0");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        JSONArray jsonarray = null;
        CloseableHttpResponse response = httpClient.execute(request);
        return response;
    }
}