package com.infodales.aem.core.service.impl;

public interface ApiConfigIDmediaService {

    public String getProtocol();
    public String getHostName();
    public String getRegistrationAPI();

    public String getLoginAPI();
    public String getTokenValidationAPI();
    public String getUserProfileAPI();
    public String getForgetPasswordAPI();
    public String getProfileUpdateAPI();

}
