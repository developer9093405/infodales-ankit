package com.infodales.aem.core.models;
import com.day.cq.dam.api.Asset;
import com.infodales.aem.core.service.impl.HelloWorldService;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;
@Model(adaptables = Resource.class
        ,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = AssetHandelModel.class)
public class AssetHandelModel {
    @Inject
    private String label;
    public String getLabel() {
        return label;
    }
    @OSGiService
    private HelloWorldService helloWorldService;
    private String message;
    @Inject
    ResourceResolver resolver;
    @Inject
    private List<String> pdflinks;
    private List<AssetHandlerPojo> assetHandlerPojoList;
    public List<AssetHandlerPojo> getAssetHandlerPojoList() {
        return assetHandlerPojoList;
    }
   private final Logger logger = LoggerFactory.getLogger(getClass());
    @PostConstruct
    protected void init(){
            message=helloWorldService.helloWorld();
        if(pdflinks!=null){
            assetHandlerPojoList =new ArrayList<AssetHandlerPojo>();
            for (String links:pdflinks){
                AssetHandlerPojo handlerPojo=new AssetHandlerPojo();
                Resource resource=resolver.getResource(links);
                Asset asset= resource.adaptTo(Asset.class);
//                logger.info("Last Modify Value  1: "+asset.getMetadataValue("jcr:lastModified"));
//                logger.info("Last Modify Value  2: "+asset.getLastModified());
                String expiry= ( String.valueOf( asset.getMetadataValue("jcr:lastModified")));
                handlerPojo.setExpiryDate(expiry);
//                logger.info("Expiry: "+expiry);
                handlerPojo.setExpiredFileDate(asset.getLastModified());
                handlerPojo.setTitle(String.valueOf(asset.getMetadataValue("dc:title")));
                handlerPojo.setDescription(String.valueOf(asset.getMetadataValue("dc:description")));
                assetHandlerPojoList.add(handlerPojo);
            }
        }
    }
    public String getMessage() {
        return message;
    }
}