package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Check Second Approval Required or Not"})
public class SecondApproverRequiredCheckProcess implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        logger.debug("Check Second Approval Required or Not Process {}", workItem.getWorkflowData().getPayload());
        MetaDataMap workflowMetadataMap = workItem.getWorkflowData().getMetaDataMap();
        if (workflowMetadataMap.containsKey(TCBConstants.SECOND_APPORVER)) {
            String secondApprover = workflowMetadataMap.get(TCBConstants.SECOND_APPORVER, String.class);
            workItem.getWorkflowData().getMetaDataMap().put(TCBConstants.SECOND_APPORVER, secondApprover);
        }
    }
}
