package com.infodales.aem.core.models;
import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = PageLinkModel.class)
public class PageLinkModel {
    @Inject
    private String label;
    @Inject
    private String description;

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    @Inject
   private ResourceResolver resolver;
    @Inject
    private List < String > pageLink;
    private List < PageLinkPojo > pageLinkList;

    public List < PageLinkPojo > getPageLinkList() {
        return pageLinkList;
    }

    @PostConstruct
    protected void init() {
        if (pageLink != null) {
            pageLinkList = new ArrayList <PageLinkPojo> ();
            for (String s1: pageLink) {
                PageLinkPojo linkPojo = new PageLinkPojo();
                Resource resource = resolver.getResource(s1);
                Page page = resource.adaptTo(Page.class);
                linkPojo.setPageLinks(s1);
                linkPojo.setPageTitles(page.getTitle());
                pageLinkList.add(linkPojo);
            }
        }
    }
}