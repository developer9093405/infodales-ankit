package com.infodales.aem.core.service.impl;

import org.apache.http.HttpEntity;

import java.io.IOException;

public interface UserProfileService {

    public HttpEntity getUserProfile(String email) throws IOException;


}
