package com.infodales.aem.core.models;

import com.adobe.cq.dam.cfm.ContentFragment;

import org.apache.sling.api.resource.Resource;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;


import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;



@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = ContentFragmentModel.class)
public class ContentFragmentModel {
    @Inject
    private List<String> cfLink;
//    @Inject
//    private Page currentPage;
//
    private List<ContentFragmentPojo> cfList;

    public List<ContentFragmentPojo> getCfList() {
        return cfList;
    }
//
//
//    @PostConstruct
//    public void init() {
//        cfList=new ArrayList<ContentFragmentPojo>();
//        if (cfList != null){
//
//            for (String  path:cfLink) {
//                ContentFragment contentFragment = currentPage.getContentResource(path).adaptTo(ContentFragment.class);
//
//                if(contentFragment !=null){
//                    FragmentTemplate fragmentTemplate = contentFragment.getTemplate();
//
//                    ContentFragmentPojo cfp= new ContentFragmentPojo();
//                }
//
//            }
//
//        }
//    }
    @Inject
    ResourceResolver resolver;
    @PostConstruct
    public void init() {
        if (cfLink != null){
            cfList=new ArrayList<ContentFragmentPojo>();
            for (String path:cfLink) {
                ContentFragmentPojo cfp= new ContentFragmentPojo();
                Resource contentFragmentResource = resolver.getResource(path);
                ContentFragment contentFragment= contentFragmentResource.adaptTo(ContentFragment.class);
//                ContentElement contentElement = contentFragment.getElements().next();
                cfp.setStudentName(contentFragment.getElement("studentName").getContent().toString());
                cfp.setStudentClass(contentFragment.getElement("studentClass").getContent().toString());
                cfp.setStudentRollno(contentFragment.getElement("studentRollno").getContent().toString());
                cfp.setStudentBranch(contentFragment.getElement("studentBranch").getContent().toString());
                cfList.add(cfp);
            }


        }


    }
}
