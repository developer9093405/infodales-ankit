package com.infodales.aem.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=/bin/myapi",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        })
public class MyApiServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Reference
    private HttpClient httpClient;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        // Set custom headers
//        Map<String, String> headers = new HashMap<>();
////        headers.put("Authorization", "Bearer <access_token>");
//        headers.put("Content-Type", "application/json");
        final List<Header> headerList=new ArrayList<>();
        headerList.add(new BasicHeader("Content-Type", "application/json"));
        int value = 2;
        // Build HTTP request
        String apiUrl = "http://192.168.0.186:8080/FetchOneData?id='"+value+"'";
        HttpGet httpRequest = new HttpGet(apiUrl);
//        for (Map.Entry<String, String> header : headers.entrySet()) {
//            httpRequest.setHeader(header.getKey(), header.getValue());
//        }
        for (Header header : headerList) {
            httpRequest.setHeader(header);
        }
        // Send HTTP request and get response
        HttpResponse httpResponse = httpClient.execute(httpRequest);
        HttpEntity entity = httpResponse.getEntity();
        String responseText = EntityUtils.toString(entity);

        // Write response back to client
        response.getWriter().write(responseText);
    }

}
// Ajax Call
//    $(function () {
//        var customer = {contact_name :"Scott",company_name:"HP"};
//        $.ajax({
//                type: "POST",
//                data :JSON.stringify(customer),
//                url: "api/Customer",
//                contentType: "application/json"
//    });
//    });
