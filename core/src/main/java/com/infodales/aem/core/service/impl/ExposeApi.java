package com.infodales.aem.core.service.impl;

public interface ExposeApi {
    public String getName();
    public String getEmail();
    public Integer getId();
}
