package com.infodales.aem.core.service;
import com.infodales.aem.core.service.impl.ExposeApi;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import javax.annotation.PostConstruct;
import java.io.IOException;
@Component(service = ExposeApi.class)
public class ExposeApiService implements ExposeApi {
    private String email;
    private Integer id=-1;
    private String name;
    @PostConstruct
    protected void init(){
        try{
            // Send a GET request to the API
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("http://localhost:8080/FetchOneData?id=2");
            HttpResponse response = client.execute(request);
            // Extract the JSON response and map it to properties of the Sling model
            HttpEntity entity = response.getEntity();
            String jsonString = EntityUtils.toString(entity, "UTF-8");
            JSONObject jsonObject = new JSONObject(jsonString);
            this.name = jsonObject.getString("name");
            this.email = jsonObject.getString("email");
            this.id=jsonObject.getInt("id");
        }
        catch (ClientProtocolException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public String getEmail() {
        return this.email;
    }
    @Override
    public Integer getId() {
        return this.id;
    }
}
