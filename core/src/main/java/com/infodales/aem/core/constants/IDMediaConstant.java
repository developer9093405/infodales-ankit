package com.infodales.aem.core.constants;

public class IDMediaConstant {

    public static final String EMAIL = "email";
    public static final String TOKEN = "token";
    public static final String UID = "uid";
    public static final String PASSWORD = "password";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String PHONENUMBER="phoneNumber";
    public static final String GENDER = "gender";
    public static final String DOB="dob";
    public static final String COUNTRY = "country";
    public static final String SUBSCRIPTION_ID = "subscription_id";
}
