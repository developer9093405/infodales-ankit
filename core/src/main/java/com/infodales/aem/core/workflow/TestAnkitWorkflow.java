package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.model.WorkflowModel;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import java.io.IOException;

@Component(service = Servlet.class,

property = {
        "sling.servlet.methods=" + HttpConstants.METHOD_POST,
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.paths=/bin/testWorkflow"
}
)
public class TestAnkitWorkflow extends SlingAllMethodsServlet {
    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    String status="Workflow  is executing";
    public static final Logger LOG= LoggerFactory.getLogger(TestAnkitWorkflow.class);
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

        final ResourceResolver resolver = request.getResourceResolver();

        String payload=request.getRequestParameter("page").toString();
        try {
            if (StringUtils.isNotBlank(payload)){
                WorkflowSession workflowSession = resolver.adaptTo(WorkflowSession.class);
                WorkflowModel workflowModel = workflowSession.getModel("/var/workflow/models/ankit-test-workfolw");

                WorkflowData workflowData = workflowSession.newWorkflowData("JCR_PATH", payload);
                status=   workflowSession.startWorkflow(workflowModel,workflowData).getState();

            }

        }catch (Exception e){

            LOG.info("\n ERROR IN WORKFLOW{} ",e.getMessage());
        }
        response .setContentType("application/json");
        response.getWriter().write(status);

    }
}
