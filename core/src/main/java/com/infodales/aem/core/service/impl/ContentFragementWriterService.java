package com.infodales.aem.core.service.impl;

import com.adobe.cq.dam.cfm.ContentFragmentException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.sling.api.SlingHttpServletRequest;

import java.io.IOException;

public interface ContentFragementWriterService {

    public void cfWriter(CloseableHttpResponse response, SlingHttpServletRequest req) throws IOException, ContentFragmentException;
    public CloseableHttpResponse hittingApi(String parameter) throws IOException;
}
