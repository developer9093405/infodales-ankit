package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.ParticipantStepChooser;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = ParticipantStepChooser.class,
        property = {"chooser.label = Second Approver Participant Chooser"})
public class SecondApproverParticipantStep implements ParticipantStepChooser {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getParticipant(final WorkItem workItem, final WorkflowSession workflowSession,
                                 final MetaDataMap metaDataMap) throws WorkflowException {

        logger.debug("Second Approver Participant Chooser {}", workItem.getWorkflowData().getPayload());

        String secondApprover = StringUtils.EMPTY;
        MetaDataMap workflowMetadataMap = workItem.getWorkflowData().getMetaDataMap();
        if (workflowMetadataMap.containsKey(TCBConstants.SECOND_APPORVER)) {
            secondApprover = workflowMetadataMap.get(TCBConstants.SECOND_APPORVER, String.class);
        }
        return secondApprover;
    }
}
