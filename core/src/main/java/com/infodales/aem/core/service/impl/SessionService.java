package com.infodales.aem.core.service.impl;

import org.apache.sling.api.SlingHttpServletRequest;

public interface SessionService {
    public String getSessionEmail(SlingHttpServletRequest request);
    public String getSessionToken(SlingHttpServletRequest request);
}
