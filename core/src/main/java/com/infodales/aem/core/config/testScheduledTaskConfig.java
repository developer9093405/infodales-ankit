package com.infodales.aem.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(
        name = "infodales-ankit SchedulerTest",
        description = "Scheduler test and created by Ankit"
)
public @interface testScheduledTaskConfig {
    @AttributeDefinition(
            name = "Scheduler Name",
            description = "Enter Scheduler name",
            type = AttributeType.STRING
    )
    public  String getSchedulerName() default "Custom Sling Scheduler Configuration";
    @AttributeDefinition(
            name = "Cron Expression",
            description = "Enter Cron Expression",
            type = AttributeType.STRING
    )
    public String getCronExpression() default "0/10 0 0 ? * * *";
}
