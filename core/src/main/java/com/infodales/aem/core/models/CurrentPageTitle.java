package com.infodales.aem.core.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = SlingHttpServletRequest.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CurrentPageTitle {
    @Inject
    SlingHttpServletRequest request;
//    ResourceResolver resolver = request.getResourceResolver();
    @Inject
    private PageManager pageManager;
    private String pageTitle;
    @PostConstruct
    protected void init() {
        Resource resource1=request.getResource();
        Page currentPage = pageManager.getContainingPage(resource1);
        if (currentPage != null) {
            pageTitle = currentPage.getTitle();
        }
    }
    public String getPageTitle() {
        return pageTitle;
    }

}
