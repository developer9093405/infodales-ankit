package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.ParticipantStepChooser;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = ParticipantStepChooser.class,
        property = {"chooser.label = First Approver Participant Chooser"})
public class FirstApproverParticipantStep implements ParticipantStepChooser {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getParticipant(final WorkItem workItem, final WorkflowSession workflowSession,
                                 final MetaDataMap metaDataMap) throws WorkflowException {

        logger.debug("First Approver Participant Step for payload {}", workItem.getWorkflowData().getPayload());

        String firstApprover = StringUtils.EMPTY;
        MetaDataMap workflowMetaDataMap = workItem.getWorkflowData().getMetaDataMap();
        if (workflowMetaDataMap.containsKey(TCBConstants.FIRST_APPORVER)) {
            firstApprover = workflowMetaDataMap.get(TCBConstants.FIRST_APPORVER, String.class);
        }
        return firstApprover;
    }
}
