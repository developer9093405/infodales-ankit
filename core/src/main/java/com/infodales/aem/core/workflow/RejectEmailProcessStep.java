package com.infodales.aem.core.workflow;


import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;

import com.infodales.aem.core.constants.TCBConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Reject Publish Email Process Step"})
public class RejectEmailProcessStep implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Reference
    private WorkflowNotificationService workflowNotificationService;

    @Reference
    private EmailService emailService;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Reject Publish Email Process Step {}", payload);
        MetaDataMap wfMetaData = workItem.getWorkflowData().getMetaDataMap();
        String initiator = workItem.getWorkflow().getInitiator();

        boolean mailEnable = false;
        if (wfMetaData.containsKey(TCBConstants.EMAIL_ENABLE)) {
            mailEnable = wfMetaData.get(TCBConstants.EMAIL_ENABLE, Boolean.class);
        }

        String workflowTitle = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.WORKFLOW_TITLE)) {
            workflowTitle = wfMetaData.get(TCBConstants.WORKFLOW_TITLE, String.class);
        }

        String processArgs = metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class);
        String[] argumentArr = processArgs.split(TCBConstants.COMMA);
        String approver = argumentArr[0];
        String argumentValue = approver.split(TCBConstants.EQUALS)[1];
        String approverName = wfMetaData.get(argumentValue, String.class);
        String titleEntry = argumentArr[1];
        String title = titleEntry.split(TCBConstants.EQUALS)[1];
        String subjectEntry = argumentArr[2];
        String subject = subjectEntry.split(TCBConstants.EQUALS)[1];

        String emailTitle;
        if (argumentValue.equals(TCBConstants.FIRST_APPORVER)) {
            emailTitle = title + " Rejected by First Approver : " + approverName;
        } else {
            emailTitle = title + " Rejected by Second Approver : " + approverName;
        }

        String emailBody = TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle;
        String email = wfMetaData.get(TCBConstants.USER_EMAIL, String.class);

        ResourceResolver resolver = workflowSession.adaptTo(ResourceResolver.class);
        if (StringUtils.isNotBlank(initiator)) {
            workflowNotificationService.inboxNotification(resolver, initiator, emailTitle, emailBody, payload);
        }

        if (mailEnable && StringUtils.isNotBlank(email)) {
            workflowNotificationService.emailNotifications(payload, workflowTitle,
                    approverName, subject, emailTitle, email);
        }
    }
}
