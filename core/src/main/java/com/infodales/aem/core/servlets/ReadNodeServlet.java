package com.infodales.aem.core.servlets;
import com.infodales.aem.core.service.impl.HelloWorldService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
@Component(service = Servlet.class,property = {
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.paths="+"/bin/getNodeJson",
        "sling.servlet.extensions=" + "txt"
})
public class ReadNodeServlet extends SlingAllMethodsServlet {
    @Reference
    HelloWorldService helloWorldService;
    @Override
    protected void doGet( SlingHttpServletRequest request,
                          SlingHttpServletResponse response) throws ServletException, IOException {
        //  String path = request.getParameter("/content/dam/infodales-ankit/test");
//        String path = request.getParameter("path");
        String path = "/content/dam/infodales-ankit/test";
        ResourceResolver resolver = request.getResourceResolver();
        Resource resource = resolver.getResource(path);
        Node node =  resource.adaptTo(Node.class);
        String value1="";
        String[] value2=null;
        String value3="";
        boolean value4=false;
        try {
            if(node.hasProperty("name")){
                value1= node.getProperty("name").getString();
            }
            if(node.hasProperty("name")){
                Property property = node.getProperty("studentnames");
                if(property.isMultiple()){
                    Value[] valueArr = property.getValues();
                    value2 = new String[valueArr.length];
                    for(int i=0; i<valueArr.length;i++){
                        value2[i] =valueArr[i].getString();
                    }
                }else{
                    value2 = new String[1];
                    value2[0]= property.getString();
                }
            }
            if(node.hasProperty("phoneno")){
                value3= node.getProperty("phoneno").getString();
            }
            if(node.hasProperty("men")){
                value4= node.getProperty("men").getBoolean();
            }
        }catch (Exception e){
            System.out.println(e);
        }
        /*ValueMap valueMap = resource.getValueMap();
        String value1 =valueMap.get("name",String.class);
        String[] value2=valueMap.get("studentnames",String[].class);
        Long value3=valueMap.get("phoneno", Long.class);
        Boolean value4=valueMap.get("men", Boolean.class);*/
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name",value1);
            jsonObject.put("studentnames",value2);
            jsonObject.put("phoneno",value3);
            jsonObject.put("men",value4);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        response.setContentType("application/json");
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonObject.toString());
        response.getWriter().write(helloWorldService.helloWorld());
//        response.getWriter().write(value);
    }
}
