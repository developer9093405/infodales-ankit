package com.infodales.aem.core.pojo;
import lombok.Getter;
import lombok.Setter;

public class Branches {

    @Getter @Setter String id;
    @Getter @Setter String name;
    @Getter @Setter String code;
    @Getter @Setter String locate;
    @Getter @Setter String locale;
    @Getter @Setter String published_at;
    @Getter @Setter String stt;
    @Getter @Setter String unitCode;
    @Getter @Setter String shortName;
    @Getter @Setter String zone;
    @Getter @Setter String area;
    @Getter @Setter String region;
    @Getter @Setter String CNME;
    @Getter @Setter String CNNHNN;
    @Getter @Setter String street;
    @Getter @Setter String NHNNArchitecture;
    @Getter @Setter String status;
    @Getter @Setter String internalArchitecture;
    @Getter @Setter String phone;
    @Getter @Setter String address;
    @Getter @Setter String lat;
    @Getter @Setter String myLong;
    @Getter @Setter String webName;
}