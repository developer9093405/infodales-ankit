package com.infodales.aem.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "infodales ankit - modular OSGi configuration",
description = "OSGi configuration for demo")
public @interface ankitOSGiConfig {
    @AttributeDefinition(
            name = "HostName",
            description = "Enter the host name",
            type = AttributeType.STRING)
    String hostName() default "localhost";
    @AttributeDefinition(
            name = "PortNo",
            description = "Enter the Port number",
            type = AttributeType.INTEGER)
    int portNo() default 8080;
    @AttributeDefinition(
            name = "HostUniqueID",
            description = "Enter the unique ID for validation",
            type = AttributeType.STRING)
    String hostUniqueID() default "root-infodales01254";

}
