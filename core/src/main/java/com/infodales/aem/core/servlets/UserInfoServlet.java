package com.infodales.aem.core.servlets;

import com.infodales.aem.core.service.impl.UserInfoService;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.resourceTypes=" + "infodales-ankit/components/content/userinfocomponent",
                //        "sling.servlet.selectors=" + "value1",
                //        "sling.servlet.extensions=" + "json"
        })
public class UserInfoServlet extends SlingSafeMethodsServlet {
    @Reference
    UserInfoService userInfoService;

    @Override
    protected void doGet(SlingHttpServletRequest request,
                         SlingHttpServletResponse response) throws ServletException, IOException {
        String user=request.getParameter("user");
        response.getWriter().write( userInfoService.getUserInformation(user).toString());

    }
}
//    String content = EntityUtils.toString(httpEntity);
