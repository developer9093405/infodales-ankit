package com.infodales.aem.core.util;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Reference;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class GetSystemUserResolver {
    public GetSystemUserResolver() {

    }

    public  static final String PROJECT_SERVICE_USER="myprojectserviceuser";


    public static ResourceResolver getSystemUserResolver(ResourceResolverFactory resolverFactory) throws LoginException {
        final Map<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put(ResourceResolverFactory.SUBSERVICE,PROJECT_SERVICE_USER);
        ResourceResolver resolver = resolverFactory.getResourceResolver(parameterMap);
        return resolver;
    }
}
