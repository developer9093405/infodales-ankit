package com.infodales.aem.core.servlets;

import com.adobe.cq.dam.cfm.ContentFragmentException;
import com.infodales.aem.core.pojo.Branches;

import com.infodales.aem.core.service.impl.ContentFragementWriterService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;


@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" + "/bin/ContentFragmentWriter"
        })
public class JSONServlet extends SlingSafeMethodsServlet {
    private static final long serialVersionUID = 1L;
    static Logger log = Logger.getLogger(JSONServlet.class);
    Branches branches;

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Reference
    ContentFragementWriterService contentFragementWriterService;

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException {
        String parameter = req.getParameter("endPoint");
        CloseableHttpResponse response = null;
        try {
            resp.setContentType("text/plain");
            if(parameter.equals("branches")){
                response=contentFragementWriterService.hittingApi(parameter);
                contentFragementWriterService.cfWriter(response,req);
                resp.getWriter().write("node added successfully");
            }else{
                resp.getWriter().write(("wrong url"));
            }


        } catch (ContentFragmentException | IOException e) {
            log.info("Author content creation failure : " + e.getMessage());
        } finally {
            if(response!=null){
                response.close();
            }
        }


    }
}