package com.infodales.aem.core.models;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.api.resource.Resource;

import javax.inject.Inject;


@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = NavbarElementModel.class)
public class    NavbarElementModel {
    @Inject
    private String navelement;

    @Inject
    private String navlink;

    public String getNavlink() {
        return navlink;
    }

    public String getNavelement() {
        return navelement;
    }
}
