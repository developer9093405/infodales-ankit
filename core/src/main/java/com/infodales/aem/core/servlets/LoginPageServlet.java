package com.infodales.aem.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;


import javax.servlet.Servlet;
import java.io.IOException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(
        service = {Servlet.class},
  property={

          "sling.servlet.methods=" + HttpConstants.METHOD_POST,
          "sling.servlet.resourceTypes="+ "infodales-ankit/components/content/login_page",
          "sling.servlet.selectors=" + "value1",
          "sling.servlet.extensions=" + "json"
//          "sling.servlet.paths=/bin/loginvalue"

          })

public class    LoginPageServlet extends SlingAllMethodsServlet {
    private static final Log LOG = LogFactory.getLog(LoginPageServlet.class);
    private static final Logger logger = (Logger) LoggerFactory.getLogger(LoginPageServlet.class);
    private static final long serialVersionUID = 1L;
  protected void doPost(final SlingHttpServletRequest request,
                       final SlingHttpServletResponse resp) throws IOException
  {
      // Get the login ID from the request parameters
      String loginId = request.getParameter("loginId");
      logger.info("loginId: " + loginId);

      // Get the password from the request parameters
      String password = request.getParameter("password");
      logger.info("password: " + password);


//      final Resource resource = request.getResource();
//      resp.getWriter().write("PAGETITLE:-"+ resource.getValueMap().get(JcrConstants.JCR_TITLE));
      resp.getWriter().write(loginId.toString());
      resp.getWriter().write(password.toString());

  }
}
