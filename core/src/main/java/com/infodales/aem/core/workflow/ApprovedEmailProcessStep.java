package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Approved Page Publish Process Step"})
public class ApprovedEmailProcessStep implements WorkflowProcess {
    @Reference
    private EmailService emailService;

    @Reference
    private WorkflowNotificationService workflowNotificationService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Approved Page Publish Process Step {}", payload);
        String initiator = workItem.getWorkflow().getInitiator();
        MetaDataMap wfMetaData = workItem.getWorkflowData().getMetaDataMap();
        boolean mailEnable = false;
        if (wfMetaData.containsKey(TCBConstants.EMAIL_ENABLE)) {
            mailEnable = wfMetaData.get(TCBConstants.EMAIL_ENABLE, Boolean.class);
        }

        String workflowTitle = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.WORKFLOW_TITLE)) {
            workflowTitle = wfMetaData.get(TCBConstants.WORKFLOW_TITLE, String.class);
        }

        String processArgs = metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class);
        String[] argumentArr = processArgs.split(TCBConstants.COMMA);
        String approver = argumentArr[0];
        String argumentValue = approver.split(TCBConstants.EQUALS)[1];
        String approverName = wfMetaData.get(argumentValue, String.class);
        String titleEntry = argumentArr[1];
        String title = titleEntry.split(TCBConstants.EQUALS)[1];
        String subjectEntry = argumentArr[2];
        String subject = subjectEntry.split(TCBConstants.EQUALS)[1];

        String emailTitle;

        if (argumentValue.equals(TCBConstants.FIRST_APPORVER)) {
            emailTitle = title + " Approved by First Approver : " + approverName;
        } else {
            emailTitle = title + " Approved by Second Approver : " + approverName;
        }
        String emailBody = TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle;

        ResourceResolver resolver = workflowSession.adaptTo(ResourceResolver.class);
        if (StringUtils.isNotBlank(initiator)) {
            workflowNotificationService.inboxNotification(resolver, initiator, emailTitle, emailBody, payload);
        }
        String email = wfMetaData.get(TCBConstants.USER_EMAIL, String.class);

        if (mailEnable && StringUtils.isNotBlank(email)) {
            workflowNotificationService.emailNotifications(payload, workflowTitle,
                    approverName, subject, emailTitle, email);
        }
    }
}
