package com.infodales.aem.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(
        name = "IDMedia- API OSGi configuration",
        description = "configuration for API"
)
public @interface apiConfigIDMedia {
    @AttributeDefinition(
            name = "Protocol",
            description = "Enter the protocol",
            type = AttributeType.STRING)
    String protocol() default "http://";

    @AttributeDefinition(
            name = "HostName",
            description = "Enter the host name",
            type = AttributeType.STRING)
    String hostName() default "localhost";

    @AttributeDefinition(
            name = "Registration_API",
            description = "Enter the Registration API",
            type = AttributeType.STRING)
    String registrationAPI() default "/empty";
    @AttributeDefinition(
            name = "Login_API",
            description = "Enter the Login API",
            type = AttributeType.STRING)
    String loginAPI() default "/empty";

    @AttributeDefinition(
            name = "TokenValidation_API",
            description = "Enter the Token Validation API",
            type = AttributeType.STRING
    )
    String tokenValidationAPI() default "/empty";

    @AttributeDefinition(
            name = "GetUserProfile_API",
            description = "Enter GetUserProfile API",
            type = AttributeType.STRING
    )
    String getUserProfileAPI() default "/empty";

    @AttributeDefinition(
            name = "ForgetPassword_API",
            description = "Enter ForgetPassword API",
            type = AttributeType.STRING
    )
    String forgetPasswordAPI() default "/empty";
    @AttributeDefinition(
            name = "ProfileUpdate_API",
            description = "Enter ProfileUpdate API",
            type = AttributeType.STRING
    )
    String profileUpdateAPI() default "/empty";

}
