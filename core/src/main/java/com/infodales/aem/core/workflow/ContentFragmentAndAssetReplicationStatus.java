package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.AssetReferenceSearch;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.ReplicationStatusProvider;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component(
        service = WorkflowProcess.class,
        property = {"process.label=Content Fragment and Asset Replication Status Checker"}
)
public class ContentFragmentAndAssetReplicationStatus implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(ContentFragmentAndAssetReplicationStatus.class);

    @Reference
    private ReplicationStatusProvider replicationStatusProvider;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap)
            throws WorkflowException {

        ResourceResolver resourceResolver = workflowSession.adaptTo(ResourceResolver.class);
        String payload = workItem.getWorkflowData().getPayload().toString();

        Resource pageResource = resourceResolver.getResource(payload);
        if (pageResource != null) {
            List<String> contentFragmentPaths = new ArrayList<>();
            List<String> assetPaths = new ArrayList<>();

            AssetReferenceSearch assetReferenceSearch = new AssetReferenceSearch(
                    pageResource.adaptTo(Node.class), DamConstants.MOUNTPOINT_ASSETS,
                    pageResource.getResourceResolver());

            Map<String, Asset> assetReferences = assetReferenceSearch.search();
            for (String assetPath : assetReferences.keySet()) {
                Resource assetResource = pageResource.getResourceResolver().getResource(assetPath);
                if (assetResource != null) {
                    if (isContentFragment(assetResource)) {
                        contentFragmentPaths.add(assetPath);
                    } else {
                        assetPaths.add(assetPath);
                    }
                }
            }

//            logger.info("content fragment path : {}", contentFragmentPaths);
//            logger.info("content fragment path : {}", assetPaths);
            checkUnpublishedContentFragments(contentFragmentPaths, resourceResolver);
            checkUnpublishedAssets(assetPaths, resourceResolver);
        }
    }

    private boolean isContentFragment(final Resource resource) {
        Node node = resource.adaptTo(Node.class);
        if (node != null) {
            try {
                if (node.hasNode("jcr:content")) {
                    Node jcrContentNode = node.getNode("jcr:content");
                    if (jcrContentNode.hasProperty("contentFragment")) {
                        return jcrContentNode.getProperty("contentFragment").getBoolean();
                    }
                }
            } catch (Exception e) {
                logger.error("Error checking content fragment property: {}", e.getMessage());
            }
        }
        return false;
    }

    private void checkUnpublishedContentFragments(List<String> contentFragmentPaths, ResourceResolver resourceResolver) {
        for (String path : contentFragmentPaths) {
            Resource resource = resourceResolver.getResource(path);
            ReplicationStatus replicationStatus = replicationStatusProvider.getReplicationStatus(resource);
            if (!replicationStatus.isActivated()) {
                logger.info("Unpublished Content Fragment: {}", path);
            }
        }
    }

    private void checkUnpublishedAssets(List<String> assetPaths, ResourceResolver resourceResolver) {
        for (String path : assetPaths) {
            Resource resource = resourceResolver.getResource(path);
            ReplicationStatus replicationStatus = replicationStatusProvider.getReplicationStatus(resource);
            if (!replicationStatus.isActivated()) {
                logger.info("Unpublished Asset: {}", path);
            }
        }
    }
}
