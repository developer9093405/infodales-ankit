package com.infodales.aem.core.models;
import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = NodeCreateModel.class)
public class NodeCreateModel {
    @Inject
    ResourceResolver resolver;

    @PostConstruct
    protected void init() {
        String parentPath = "/content/dam/infodales-ankit";
        Resource infodalesResource = resolver.getResource(parentPath);
        if (infodalesResource != null) {
            Node parentNode = infodalesResource.adaptTo(Node.class);
            try {
                if (parentNode != null && !parentNode.hasNode("test")) {

                    Node newNode = parentNode.addNode("test", JcrConstants.NT_UNSTRUCTURED);
                    newNode.setProperty("name", "testnode");
                    newNode.setProperty("men", true);
                    newNode.setProperty("phoneno", 960775352);
                    newNode.setProperty("bhai","yesBoos");
                    String[] propertyValue = {"ankit", "aman", "Ram", "Krishna"};
                    newNode.setProperty("studentnames", propertyValue);
                    newNode.getSession().save();
                }
            } catch (RepositoryException e ) {

            }

        }
    }
}

