package com.infodales.aem.core.service;
import com.infodales.aem.core.service.impl.HelloWorldService;
import org.osgi.service.component.annotations.Component;
@Component(service = HelloWorldService.class)
public class HelloWorldServiceImpl implements HelloWorldService {

    @Override
    public String helloWorld() {
        return " Hello World Ankit";
    }
}
