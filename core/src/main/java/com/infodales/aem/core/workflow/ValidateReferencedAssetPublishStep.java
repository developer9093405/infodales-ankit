package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.AssetReferenceSearch;
import com.day.cq.replication.ReplicationStatus;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Validate Referenced Asset Publish"})
public class ValidateReferencedAssetPublishStep implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Validate Referenced Asset Publish {} ", payload);

        MetaDataMap workflowMetadataMap = workItem.getWorkflowData().getMetaDataMap();
        ResourceResolver resourceResolver = workflowSession.adaptTo(ResourceResolver.class);
        if (StringUtils.startsWith(payload, TCBConstants.CONTENT_TECHCOMBANK_PATH)) {
            logger.debug("Payload is a Asset {}", payload);
            Resource resource = resourceResolver.getResource(payload);
            Node node = resource.adaptTo(Node.class);
            AssetReferenceSearch ref = new AssetReferenceSearch(node, DamConstants.MOUNTPOINT_ASSETS, resourceResolver);
            Map<String, Asset> allref = new HashMap<>();

            List<String> unPublishedAsset = new ArrayList<>();
            allref.putAll(ref.search());
            for (Map.Entry<String, Asset> entry : allref.entrySet()) {
                String val = entry.getKey();
                Resource assetResource = resourceResolver.getResource(val);
                ReplicationStatus replicationStatus = assetResource.adaptTo(ReplicationStatus.class);
                if (!replicationStatus.isActivated()) {
                    unPublishedAsset.add(val);
                }
            }
            if (!unPublishedAsset.isEmpty()) {
                workflowMetadataMap.put(TCBConstants.ASSET_PUBLISHED, TCBConstants.FALSE);
                workflowMetadataMap.put(TCBConstants.ASSET_REFERENCE, unPublishedAsset);
            } else {
                workflowMetadataMap.put(TCBConstants.ASSET_PUBLISHED, TCBConstants.TRUE);
            }
        } else {
            logger.debug("Payload is a Asset {}", payload);
            workflowMetadataMap.put(TCBConstants.ASSET_PUBLISHED, TCBConstants.TRUE);
        }
    }
}
