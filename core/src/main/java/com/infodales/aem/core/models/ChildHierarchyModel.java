package com.infodales.aem.core.models;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(adaptables = Resource.class)
public class    ChildHierarchyModel {

    @Self
    private Resource resource;

    private List<String> childPageList;
    public List<String> getChildPageList() {
        return childPageList;
    }

    @PostConstruct
    protected void init() {
        childPageList= new ArrayList<String>();
//        String path = resource.getPath();
        Resource pageResource = resource.getParent();

        Page page = pageResource.adaptTo(Page.class);
        Iterator<Page> children = page.listChildren();
        while (children.hasNext()) {
         Page childPage= children.next();
            childPageList.add(childPage.getTitle());
        }
    }
}
