package com.infodales.aem.core.service;

import com.infodales.aem.core.bean.UserInfoPojo;
import com.infodales.aem.core.service.impl.UserInfoService;
import org.osgi.service.component.annotations.Component;

import java.util.HashMap;
import java.util.Map;

@Component(service = UserInfoService.class,immediate = true)
public class UserInfoServiceImpl implements UserInfoService {
    @Override
    public UserInfoPojo getUserInformation(String user) {
        UserInfoPojo user1=new UserInfoPojo("Ankit","Pardhi","ankitPardhi1012gamil.com","9307753525",null);
        UserInfoPojo user2=new UserInfoPojo("Harshit","Pardhi","harshitPardhi1012gamil.com","1234569870",null);
        UserInfoPojo user3=new UserInfoPojo("Dinesh","Pardhi","DineshPardhi1012gamil.com","565565451515468424",null);

        Map<String, UserInfoPojo> userInfo = new HashMap<String, UserInfoPojo>();
        userInfo.put("Ankit", user1);
        userInfo.put("Harshit", user2);
        userInfo.put("Dinesh", user3);

        return userInfo.get(user);
    }
}
