package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Save Participant Process Step"})
public class SaveParticipantProcessStep implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private EmailService emailService;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {
        logger.debug("Save Participant Process Step {}", workItem.getWorkflowData().getPayload());

        MetaDataMap workflowMetadataMap = workItem.getMetaDataMap();
        String historyEntryPath = workflowMetadataMap.get(TCBConstants.HISTORY_ENTRY_PATH, String.class);
        MetaDataMap wfMetaData = workItem.getWorkflow().getWorkflowData().getMetaDataMap();
        ResourceResolver resourceResolver = workflowSession.adaptTo(ResourceResolver.class);
        wfMetaData.put(TCBConstants.USER_EMAIL, emailService.getUserEmail(workItem, resourceResolver));

        if (StringUtils.isNotBlank(historyEntryPath)) {
            ResourceResolver resolver = workflowSession.adaptTo(ResourceResolver.class);
            Resource resource = resolver.getResource(historyEntryPath + TCBConstants.WORKITEM_METADATA);
            if (!Objects.isNull(resource)) {
                ValueMap valueMap = resource.getValueMap();
                if (valueMap.containsKey(TCBConstants.FIRST_APPORVER)) {
                    wfMetaData.put(TCBConstants.FIRST_APPORVER,
                            valueMap.get(TCBConstants.FIRST_APPORVER, String.class));
                }
                if (valueMap.containsKey(TCBConstants.SECOND_APPORVER)) {
                    wfMetaData.put(TCBConstants.SECOND_APPORVER,
                            valueMap.get(TCBConstants.SECOND_APPORVER, String.class));
                }
                if (valueMap.containsKey(TCBConstants.TARGET_PATH)) {
                    wfMetaData.put(TCBConstants.TARGET_PATH,
                            valueMap.get(TCBConstants.TARGET_PATH, String.class));
                }
            }
        }
    }
}
