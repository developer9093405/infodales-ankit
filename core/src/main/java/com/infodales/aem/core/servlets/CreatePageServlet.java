package com.infodales.aem.core.servlets;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = {Servlet.class},
        property = {
                "sling.servlet.paths=/bin/createNewPages",
                "sling.servlet.methods=POST"
        })

public class CreatePageServlet extends SlingAllMethodsServlet {

    @Override
    protected void doPost(SlingHttpServletRequest request,
                          SlingHttpServletResponse response) throws IOException {

        ResourceResolver resolver = request.getResourceResolver();

        String parentpath = "/content/idmedia-aem";
        String pageName = "samplepage";
        String pageTitle = "Sample page";
        String template = "/apps/idmedia-aem/templates/janhvitemplate";
        String pageComp = "/apps/idmedia-aem/components/structure/janhvipagecomp";

        Page newPage = null;

        PageManager pageManager = resolver.adaptTo(PageManager.class);

        try {
            newPage = pageManager.create(parentpath, pageName, template, pageTitle);
        } catch (WCMException e) {
            throw new RuntimeException(e);
        }

        Node pageNode = newPage.adaptTo(Node.class);
        Node jcrNode = null;

        if (newPage.hasContent()) {
            jcrNode = newPage.getContentResource().adaptTo(Node.class);
        } else {
            try {
                jcrNode = pageNode.addNode("jcr:content", "cq:PageContent");
            } catch (RepositoryException e) {
                throw new RuntimeException(e);
            }
        }

        try {
            jcrNode.setProperty("sling:resourceType", pageComp);
        } catch (RepositoryException e) {
            throw new RuntimeException(e);
        }

    }
}
