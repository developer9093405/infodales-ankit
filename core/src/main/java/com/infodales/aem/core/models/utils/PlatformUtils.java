package com.infodales.aem.core.models.utils;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.LandingPageMenuModel;
import com.techcombank.core.models.LanguageLabelModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility Class for TCB Module.
 */
public final class PlatformUtils {

    private PlatformUtils() {
    }

    /**
     * This Method Checks if link is internal and append .html to the link
     * @param resourceResolver
     * @param linkUrl
     * @return linkUrl
     */
    public static String isInternalLink(final ResourceResolver resourceResolver, final String linkUrl) {

        if (null != linkUrl
                && (resourceResolver.getResource(linkUrl) != null || linkUrl.startsWith(TCBConstants.TCB_BASE_PATH))
                && !StringUtils.endsWith(linkUrl, TCBConstants.HTMLEXTENSION)
                && !StringUtils.endsWith(linkUrl, TCBConstants.PDFEXTENSION)) {
            return linkUrl.concat(TCBConstants.HTMLEXTENSION);
        } else {
            return linkUrl;
        }
    }

    /**
     * This Method returns List of Languages and its corresponding page page for the
     * current page request
     * @param resourceResolver
     * @param currentPage
     * @return List languageDetails
     */
    public static List<LanguageLabelModel> getLanguageDetails(final ResourceResolver resourceResolver,
                                                              final Page currentPage) {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        String currentPagePath = currentPage.getPath();
        boolean isLanguagePage = false;
        List<LanguageLabelModel> languageDetails = new ArrayList<>();
        if (isTcbBasePath(currentPagePath)) {
            // EX Path:/content/techcombank/web/vn/en/personal.html
            String[] currentPageSubPaths = currentPagePath.split(TCBConstants.SLASH);
            // Country: Vn
            String country = currentPageSubPaths[TCBConstants.COUNTRY_INDEX];
            // EX Path: /content/techcombank/web/vn
            String countryPath = TCBConstants.TCB_BASE_PATH + country;
            Resource countryPage = resourceResolver.getResource(countryPath);
            for (Resource languageRes : countryPage.getChildren()) {
                if (!languageRes.getPath().contains(JcrConstants.JCR_CONTENT)) {
                    isLanguagePage = true;
                    String[] languageSubPaths = languageRes.getPath().split(TCBConstants.SLASH);
                    String language = languageSubPaths.length >= TCBConstants.LANGUAGE_INDEX
                            ? languageSubPaths[TCBConstants.LANGUAGE_INDEX]
                            : StringUtils.EMPTY;
                    LanguageLabelModel languageDetail = new LanguageLabelModel();
                    String languageTitle = getLanguageTitle(languageRes, isLanguagePage, resourceResolver);
                    if (StringUtils.isNotBlank(languageTitle)) {
                        String aliaspageURL = getAliasPagePath(country, language, currentPageSubPaths, pageManager);
                        languageDetail.setLanguage(languageTitle);
                        languageDetail.setLanguageUrl(aliaspageURL);
                        languageDetails.add(languageDetail);
                    }
                }
            }

        }
        return languageDetails;
    }

    /**
     * This Method return Language Title for the given Page Path
     * @param isLanguagePage
     * @param resourceResolver
     * @return String languageTitle
     */
    public static String getLanguageTitle(final Resource languageRes, final boolean isLanguagePage,
                                          final ResourceResolver resourceResolver) {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page languagePage;
        String languageTitle = StringUtils.EMPTY;
        if (isLanguagePage) {
            languagePage = pageManager.getContainingPage(languageRes);
            languageTitle = languagePage.getTitle();

        } else {
            String currentPagePath = languageRes.getPath();
            String[] languagePagePath = currentPagePath.split(TCBConstants.SLASH);
            if (isTcbBasePath(currentPagePath) && languagePagePath.length >= TCBConstants.LANGUAGE_INDEX) {
                String languagePath = TCBConstants.TCB_BASE_PATH + languagePagePath[TCBConstants.COUNTRY_INDEX]
                        + TCBConstants.SLASH + languagePagePath[TCBConstants.LANGUAGE_INDEX];
                languagePage = pageManager.getContainingPage(languagePath);
                languageTitle = languagePage.getTitle();
            }
        }
        return languageTitle;
    }

    /**
     * Thia Method check if path starts with TCB base path and returns true if it
     * matches
     * @param currentPagePath
     * @return boolean isValidPath
     */
    public static boolean isTcbBasePath(final String currentPagePath) {
        return currentPagePath.startsWith(TCBConstants.TCB_BASE_PATH);
    }

    /**
     * This Method Checks and returns Alias Page Path if page got authored with
     * Alias Page Property
     * @param country
     * @param language
     * @param currentPageSubPaths
     * @param pageManager
     * @return String languageurl
     */
    public static String getAliasPagePath(final String country, final String language,
                                          final String[] currentPageSubPaths, final PageManager pageManager) {
        String languageurl = TCBConstants.TCB_BASE_PATH + country + TCBConstants.SLASH + language;
        String pageAliasSubPath = TCBConstants.TCB_BASE_PATH + country + TCBConstants.SLASH + language;
        int subPathSize = currentPageSubPaths.length;
        // EX Path:/content/techcombank/web/vn/en/personal.html--Path start index->6(Persoanl)
        for (int i = TCBConstants.PATH_START_INDEX; i < subPathSize; i++) {
            pageAliasSubPath = pageAliasSubPath.concat(TCBConstants.SLASH + currentPageSubPaths[i]);
            Page childAliasPage = pageManager.getContainingPage(pageAliasSubPath);
            if (null != childAliasPage) {
                ValueMap childPageProp = childAliasPage.getProperties();
                if (childPageProp.containsKey(TCBConstants.PROP_ALIAS)) {
                    String pageAliasName = childPageProp.get(TCBConstants.PROP_ALIAS).toString();
                    languageurl = languageurl.concat(TCBConstants.SLASH + pageAliasName);
                } else {
                    languageurl = languageurl.concat(TCBConstants.SLASH + currentPageSubPaths[i]);
                }
            } else {
                languageurl = languageurl.concat(TCBConstants.SLASH + currentPageSubPaths[i]);
            }
        }
        languageurl = languageurl.concat(TCBConstants.HTMLEXTENSION);
        return languageurl;
    }

    /**
     * This Method checks and returns true when All product Link is enabled on
     * Template Level
     * @param currentPage
     * @return boolean isallProductEnabled
     */
    public static boolean checkAllProductLink(final Page currentPage) {
        ValueMap pageProp = currentPage.getProperties();
        boolean isallProductEnabled = false;
        if (pageProp.containsKey(TCBConstants.CQ_TEMPLATE) && pageProp.containsKey(TCBConstants.PROP_ENABLE_ALL_PRODUCT)
                && (TCBConstants.PERSONAL_TEMPLATE_PATH
                .equalsIgnoreCase(pageProp.get(TCBConstants.CQ_TEMPLATE).toString())
                || TCBConstants.BUSINESS_TEMPLATE_PATH
                .equalsIgnoreCase(pageProp.get(TCBConstants.CQ_TEMPLATE).toString())
                || TCBConstants.CONTENT_TEMPLATE_PATH
                .equalsIgnoreCase(pageProp.get(TCBConstants.CQ_TEMPLATE).toString()))) {
            isallProductEnabled = Boolean.valueOf(pageProp.get(TCBConstants.PROP_ENABLE_ALL_PRODUCT).toString());

        }
        return isallProductEnabled;
    }

    /**
     * This Method returns MenuTitle after language page
     * @param currentPage
     * @return String MenuTitle
     */
    public static String currentPageMenuTitle(final Page currentPage) {
        String menuTitle = StringUtils.EMPTY;
        if (currentPage.getDepth() >= TCBConstants.PATH_START_INDEX) {
            // EX Path:/content/techcombank/web/vn/en/personal.html--Path start
            // index->6(Persoanl)
            menuTitle = currentPage.getPath().split(TCBConstants.SLASH)[TCBConstants.PATH_START_INDEX];
        }
        return menuTitle;
    }

    /**
     * This Method checks if current page MenuTitle is listed in the Landing Menu
     * drop down
     * @param landingPageMenu
     * @param currentPage
     * @return boolean isPageMenuListed
     */
    public static boolean currentPageMenuListed(final List<LandingPageMenuModel> landingPageMenu,
                                                final Page currentPage) {
        String menuTitle = currentPageMenuTitle(currentPage);
        boolean isPageMenuListed = false;
        for (LandingPageMenuModel pageMenuItem : landingPageMenu) {
            if (pageMenuItem.getLandingMenuTitle().equalsIgnoreCase(menuTitle)) {
                isPageMenuListed = true;
                break;
            }
        }

        return isPageMenuListed;
    }

    /**
     * This method construct and return Interaction Type string
     * @param clickInfo
     * @param type
     * @return String interactiontype
     */
    public static String getInteractionType(final String clickInfo, final String type) {
        return "{'webInteractions': {'name': '" + clickInfo + "','type': '" + type + "'}}";
    }

    /**
     * This method get the parent page properties for the give path
     * @param page
     * @return HierarchyNodeInheritanceValueMap
     */
    public static HierarchyNodeInheritanceValueMap getInheritedPageProperties(final Page page) {

        return new HierarchyNodeInheritanceValueMap(page.getContentResource());

    }

    /**
     * Check page validity
     * @param rr, pagePath
     * @return boolean on the page check
     */
    public static boolean checkPageValidity(final ResourceResolver rr, final String pagePath) {
        PageManager pm = rr.adaptTo(PageManager.class);
        if (pm != null) {
            return pm.getPage(pagePath) != null;
        }
        return false;
    }


    /**
     * Checks the Instance Mode
     */
    public static boolean isAuthor(final SlingSettingsService slingSettings) {
        return slingSettings.getRunModes().contains(TCBConstants.AUTHOR);
    }

    /**
     * Returns the domain
     */
    public static String getExternalizer(final Externalizer externalizer,
                                         final ResourceResolver resourceResolver,
                                         final SlingSettingsService slingSettings,
                                         final String sourceWebsite) {
        String domain;
        if (isAuthor(slingSettings)) {
            domain = externalizer.authorLink(resourceResolver, StringUtils.EMPTY);
        } else if (StringUtils.equalsIgnoreCase(sourceWebsite, TCBConstants.SOURCE_APP)) {
            domain = externalizer.externalLink(resourceResolver, TCBConstants.APP_LOCAL_DOMAIN, StringUtils.EMPTY);
        } else {
            domain = externalizer.publishLink(resourceResolver, StringUtils.EMPTY);
        }
        return StringUtils.endsWith(domain, TCBConstants.SLASH) ? domain.substring(0, domain.length() - 1) : domain;
    }

    /**
     * Returns the APP_DOMAIN
     */
    public static String getAppDomainExternalizer(final Externalizer externalizer,
                                                  final ResourceResolver resourceResolver) {
        String domain = externalizer.externalLink(resourceResolver, TCBConstants.APP_DOMAIN, "");
        return StringUtils.endsWith(domain, TCBConstants.SLASH) ? domain.substring(0, domain.length() - 1) : domain;
    }

    /**
     * This method return current Alias page Path
     * @param resourceResolver
     * @param currentPagePath
     * @return String aliasPagePath
     */
    public static String currentPagePathWithAlias(final ResourceResolver resourceResolver,
            final String currentPagePath) {
        // EX Path:/content/techcombank/web/vn/en/personal.html
        String[] currentPageSubPaths = currentPagePath.split(TCBConstants.SLASH);
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        // Country: Vn
        String country = currentPageSubPaths[TCBConstants.COUNTRY_INDEX];
        // Language:en
        String language = currentPageSubPaths.length >= TCBConstants.LANGUAGE_INDEX
                ? currentPageSubPaths[TCBConstants.LANGUAGE_INDEX]
                : StringUtils.EMPTY;
        return getAliasPagePath(country, language, currentPageSubPaths, pageManager);
    }

    /**
     * This Method Checks for type of the link which is used for analytics
     * @param resourceResolver
     * @param linkUrl
     * @return String type
     */
    public static String getUrlLinkType(final ResourceResolver resourceResolver, final String linkUrl) {

        String urlType = isInternalLink(resourceResolver, linkUrl);
        if (StringUtils.isBlank(urlType)) {
            return StringUtils.EMPTY;
        } else {
            if (urlType.startsWith(TCBConstants.TCB_BASE_PATH)
                    && urlType.endsWith(TCBConstants.HTMLEXTENSION)) {
                return TCBConstants.OTHER;
            } else if (urlType.endsWith(TCBConstants.PDFEXTENSION)) {
                return TCBConstants.DOWNLOAD;
            } else {
                return TCBConstants.EXIT;
            }
        }
    }

    /**
     * This method return web image rendition path
     * @param imagePath
     * @return String webImageRenditionPath
     */
    public static String getWebImagePath(final String imagePath) {
        if (StringUtils.isNotBlank(imagePath)) {
            if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_SVG_EXTENSION)) {
                return imagePath;
            } else if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_PNG_EXTENSION)) {
                return imagePath + TCBConstants.PNG_WEB_IMAGE_RENDITION_PATH;
            } else {
                return imagePath + TCBConstants.JPEG_WEB_IMAGE_RENDITION_PATH;
            }
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * This method return mobile image rendition path
     * @param imagePath
     * @return String mobileImageRenditionPath
     */
    public static String getMobileImagePath(final String imagePath) {
        if (StringUtils.isNotBlank(imagePath)) {
            if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_SVG_EXTENSION)) {
                return imagePath;
            } else if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_PNG_EXTENSION)) {
                return imagePath + TCBConstants.PNG_MOBILE_IMAGE_RENDITION_PATH;
            } else {
                return imagePath + TCBConstants.JPEG_MOBILE_IMAGE_RENDITION_PATH;
            }
        } else {
            return StringUtils.EMPTY;
        }
    }

    public static String getThumbnailImagePath(final String imagePath) {
        if (StringUtils.isNotBlank(imagePath)) {
            if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_PNG_EXTENSION)) {
                return imagePath + TCBConstants.PNG_THUMBNAIL_IMAGE_RENDITION_PATH;
            } else {
                return imagePath + TCBConstants.JPEG_THUMBNAIL_IMAGE_RENDITION_PATH;
            }
        } else {
            return StringUtils.EMPTY;
        }
    }
}
