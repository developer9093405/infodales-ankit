package com.infodales.aem.core.models;

public class NavLink {

    private String pageTitle;

    private String link;

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public String getLink() {
        return link;
    }
}
