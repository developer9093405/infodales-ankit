package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = WorkflowProcess.class, property = {"process.label = Move Error Status Process Step"})
public class MoveErrorStatusProcessStep implements WorkflowProcess {

    @Reference
    private EmailService emailService;

    @Reference
    private WorkflowNotificationService workflowNotificationService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Approved Page Publish Process Step {}", workItem.getWorkflowData().getPayload());
        MetaDataMap wfMetaData = workItem.getWorkflowData().getMetaDataMap();
        String initiator = workItem.getWorkflow().getInitiator();

        boolean mailEnable = false;
        if (wfMetaData.containsKey(TCBConstants.EMAIL_ENABLE)) {
            mailEnable = wfMetaData.get(TCBConstants.EMAIL_ENABLE, Boolean.class);
        }

        String workflowTitle = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.WORKFLOW_TITLE)) {
            workflowTitle = wfMetaData.get(TCBConstants.WORKFLOW_TITLE, String.class);
        }

        String message = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.MESSAGE)) {
            message = wfMetaData.get(TCBConstants.MESSAGE, String.class);
        }
        String emailTitle = "Error in Moving Resource";
        String emailBody = TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle;

        ResourceResolver resolver = workflowSession.adaptTo(ResourceResolver.class);

        if (StringUtils.isNotBlank(initiator)) {
            workflowNotificationService.inboxNotification(resolver, initiator, emailTitle, emailBody, payload);
        }

        String email = wfMetaData.get(TCBConstants.USER_EMAIL, String.class);

        if (mailEnable && StringUtils.isNotBlank(email)) {
            String[] recipients = {email};
            StringBuilder sb = new StringBuilder();
            sb.append("<p>" + TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle + " </p><br/>");
            sb.append(message);
            sb.append("<p>Resource Path : " + workItem.getWorkflowData().getPayload() + "</p>");
            emailService.sendEmail(emailTitle, sb.toString(), recipients);
            logger.debug("Error email Sent Successfully");
        }
    }
}
