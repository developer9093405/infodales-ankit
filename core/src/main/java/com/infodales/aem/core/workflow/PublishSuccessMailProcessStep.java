package com.infodales.aem.core.workflow;

import com.adobe.acs.commons.notifications.InboxNotificationSender;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Email Successful Publish Step"})
public class PublishSuccessMailProcessStep implements WorkflowProcess {

    @Reference
    private EmailService emailService;

    @Reference
    private WorkflowNotificationService workflowNotificationService;

    @Reference
    private InboxNotificationSender inboxNotificationSender;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Publish Success Mail Process Step {}", payload);
        MetaDataMap wfMetaData = workItem.getWorkflowData().getMetaDataMap();
        boolean mailEnable = false;
        if (wfMetaData.containsKey(TCBConstants.EMAIL_ENABLE)) {
            mailEnable = wfMetaData.get(TCBConstants.EMAIL_ENABLE, Boolean.class);
        }

        String workflowTitle = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.WORKFLOW_TITLE)) {
            workflowTitle = wfMetaData.get(TCBConstants.WORKFLOW_TITLE, String.class);
        }
        String destinationFileName = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.DESTINATION_FILE_NAME)) {
            destinationFileName = wfMetaData.get(TCBConstants.DESTINATION_FILE_NAME, String.class);
        }

        String initiator = workItem.getWorkflow().getInitiator();
        String processArgs = metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class);
        String[] argumentArr = processArgs.split(TCBConstants.COMMA);

        String titleEntry = argumentArr[0];
        String title = titleEntry.split(TCBConstants.EQUALS)[1];
        String messageEntry = argumentArr[1];
        String message = messageEntry.split(TCBConstants.EQUALS)[1];


        String emailTitle = title + " Successful";
        String emailBody = TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle;
        String contentPath = StringUtils.equals(title, TCBConstants.MOVE) ? destinationFileName : payload;

        ResourceResolver resolver = workflowSession.adaptTo(ResourceResolver.class);
        if (StringUtils.isNotBlank(initiator)) {
            workflowNotificationService.inboxNotification(resolver, initiator, emailTitle,
                    emailBody, contentPath);
        }

        String email = wfMetaData.get(TCBConstants.USER_EMAIL, String.class);
        if (mailEnable && StringUtils.isNotBlank(email)) {
            String[] recipients = {email};
            StringBuilder sb = new StringBuilder();
            sb.append("<p>" + TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle + " </p><br/>");
            sb.append("<p>" + message + "</p><br/>");
            sb.append("<p>Resource Path : " + payload + "</p>");
            emailService.sendEmail(title + " Successful", sb.toString(), recipients);
            logger.debug("{} Email Sent Successfully", title);
        }
    }
}
