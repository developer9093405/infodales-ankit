package com.infodales.aem.core.service.impl;

public interface AnkitOSGiConfigCall {

    String getHostName();
    int getPort();
    String getUniqueId();


}
