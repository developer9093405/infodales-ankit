package com.infodales.aem.core.service.impl;

import org.osgi.service.component.annotations.Component;

public interface HelloWorldService {
     String helloWorld();
}
