package com.infodales.aem.core.models;
public class AssetHandlerPojo {
    private String title;
    private String description;
    private String author;
    private String path;
    private String expiryDate;
    private  Long expiredFileDate;

    public Long getExpiredFileDate() {
        return expiredFileDate;
    }

    public void setExpiredFileDate(Long expiredFileDate) {
        this.expiredFileDate = expiredFileDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
