package com.infodales.aem.core.models;

import com.day.cq.wcm.foundation.model.Page;

import org.apache.poi.hslf.record.SoundData;
import org.apache.poi.sl.usermodel.PaintStyle.SolidPaint;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = PageLinkModel.class)
public class PageLinkPojo {
    private String pageTitles;
    

    private String pageLinks;

    public String getPageTitles() {
        return pageTitles;
      
    }

    public void setPageTitles(String pageTitles) {
        this.pageTitles = pageTitles;
    }

    public String getPageLinks() {
        return pageLinks;
    }

    public void setPageLinks(String pageLinks) {
        this.pageLinks = pageLinks;
    }
}

