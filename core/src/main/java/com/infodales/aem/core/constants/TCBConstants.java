package com.infodales.aem.core.constants;

public final class TCBConstants {
    public static final String HTMLEXTENSION = ".html";
    public static final String PDFEXTENSION = ".pdf";
    public static final String AEM_DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String DATE_DD_MM_YY_FORMAT = "dd MMMM yyyy";
    public static final String HASH = "#";
    public static final String EXIT = "exit";
    public static final String OTHER = "other";
    public static final String DOWNLOAD = "download";
    public static final String CAROUSEL_CLICK = "carouselClick";

    // Header Constants
    public static final String TCB_BASE_PATH = "/content/techcombank/web/";
    public static final String PROP_ALIAS = "sling:alias";
    public static final String SLASH = "/";
    public static final String UNDERSCORE = "_";
    public static final String PERSONAL_TEMPLATE_PATH = "/conf/techcombank/settings/wcm/templates/personal-banking";
    public static final String BUSINESS_TEMPLATE_PATH = "/conf/techcombank/settings/wcm/templates/business-banking";
    public static final String CONTENT_TEMPLATE_PATH = "/conf/techcombank/settings/wcm/templates/content-page";
    public static final String CQ_TEMPLATE = "cq:template";
    public static final String CQ_PAGE = "cq:Page";
    public static final String PROP_ENABLE_ALL_PRODUCT = "enableAllProductLink";
    public static final int COUNTRY_INDEX = 4;
    public static final int LANGUAGE_INDEX = 5;
    public static final int PATH_START_INDEX = 6;
    public static final String APPLICATION_JSON = "application/json";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BASIC = "Basic ";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String SUCCESS = "Success";
    public static final String ERROR = "Error";
    public static final String INVALID_RESOURCE_PATH = "invalid_resource_path";
    public static final String ITEMS = "items";
    public static final String ITEMSORDER = "itemsOrder";
    public static final String COLON_ITEMS = ":items";
    public static final String COLON_ITEMSORDER = ":itemsOrder";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String AUTHOR = "author";
    public static final String HEADER_XF = "headerXF";
    public static final String FOOTER_XF = "footerXF";
    public static final int CONNECTION_TIMEOUT = 10000;

    // Currency Convertor Exchange Rate Constants
    public static final String DASH = "-";
    public static final String COLON = ":";
    public static final String EXCHANGE_RATE_DATA_PATH = "/data/master";
    public static final String EXCHANGE_RATE_LATEST_UPDATED_DATE = "latestUpdatedDate";
    public static final String EXCHANGE_RATE_DATE_TIME = "%exchangeRateDateTime";
    public static final String EXCHNAGE_RATE_DAY_MONTH_FORMAT = "%02d";
    public static final String EXCHNAGE_RATE_JSON_ARRAY = "exchangeRate";
    public static final String CALCULATORS = "Calculators";
    public static final String BAR = "|";
    public static final String IMAGE_PNG_EXTENSION = ".png";
    public static final String IMAGE_SVG_EXTENSION = ".svg";
    public static final String APP_DOMAIN = "app-domain";
    public static final String APP_LOCAL_DOMAIN = "app-local-domain";
    public static final String SOURCE_APP = "app";
    public static final String IMAGE = "image";
    public static final String VALUE = "value";
    public static final String DOT = ".";

    // Nav Equal panel
    public static final String NAV_PANEL_TITLE = "Nav Equal Panel";

    public static final String PNG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.png";
    public static final String JPEG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
    public static final String PNG_MOBILE_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.640.640.png";
    public static final String PNG_THUMBNAIL_IMAGE_RENDITION_PATH =
            "/jcr:content/renditions/cq5dam.thumbnail.319.319.png";
    public static final String JPEG_MOBILE_IMAGE_RENDITION_PATH =
            "/jcr:content/renditions/cq5dam.web.640.640.jpeg";
    public static final String JPEG_THUMBNAIL_IMAGE_RENDITION_PATH =
            "/jcr:content/renditions/cq5dam.thumbnail.319.319.jpeg";

    public static final String FIRST_APPORVER = "firstApprover";
    public static final String SECOND_APPORVER = "secondApprover";
    public static final String TARGET_PATH = "targetPath";
    public static final String ASSET_PUBLISHED = "assetPublished";
    public static final String ASSET_REFERENCE = "assetReference";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_ID = "userId";
    public static final String USER_PROFILE_EMAIL_PATH = "profile/email";
    public static final String EMAIL_ENABLE = "mailEnable";
    public static final String HISTORY_ENTRY_PATH = "historyEntryPath";
    public static final String CONTENT_TECHCOMBANK_PATH = "/content/techcombank";
    public static final String WORKITEM_METADATA = "/workItem/metaData";
    public static final String PROCESS_ARGS = "PROCESS_ARGS";
    public static final String WORKFLOW_TITLE = "workflowTitle";

    public static final String EQUALS = "=";
    public static final String COMMA = ",";
    public static final String STATUS = "Status";
    public static final String MESSAGE = "Message";
    public static final String MOVE = "Move";
    public static final String DESTINATION_FILE_NAME = "destinationFileName";
    public static final String WORKFLOW_TITLE_VAL = "Workflow Title : ";
    private TCBConstants() {
    }
}
