package com.infodales.aem.core.models;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.security.PrivateKey;

@Model(adaptables = {SlingHttpServletRequest.class,Resource.class}
        ,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = AdaptationModel.class
)
public class AdaptationModel {
    String message;
    @SlingObject
    private SlingHttpServletRequest request;

    @ValueMapValue@Via("Resource")
    private String firstname;

    @ValueMapValue@Via("Resource")
    private String lastname;
    @PostConstruct
    protected void init(){
        message="hiii my name is ankit";

    }

    public String getMessage() {
        return message;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

}
