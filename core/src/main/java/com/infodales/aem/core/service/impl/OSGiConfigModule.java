package com.infodales.aem.core.service.impl;

public interface OSGiConfigModule {
    public String getHostName();

    public  int getPortNo();
    public  String getHostUniqueID();

}
