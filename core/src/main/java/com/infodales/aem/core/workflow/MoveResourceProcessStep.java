package com.infodales.aem.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Workspace;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Move Resource Process Step"})
public class MoveResourceProcessStep implements WorkflowProcess {

    @Reference
    private Replicator replicator;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {

        logger.debug("Move Resource Process Step {}", workItem.getWorkflowData().getPayload());
        MetaDataMap workflowMetadataMap = workItem.getWorkflowData().getMetaDataMap();
        MetaDataMap wfMetaData = workItem.getWorkflowData().getMetaDataMap();
        String payload = workItem.getWorkflowData().getPayload().toString();
        ResourceResolver resolver = workflowSession.adaptTo(ResourceResolver.class);
        Session session = resolver.adaptTo(Session.class);
        Resource sourceResource = resolver.getResource(payload);
        try {
            if (sourceResource == null) {
                logger.debug("Payload Path is not Valid : {}", payload);
                workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.ERROR);
                workflowMetadataMap.put(TCBConstants.MESSAGE, "Invalid Source Path :" + payload);
                return;
            }

            String destinationPath = wfMetaData.get(TCBConstants.TARGET_PATH, String.class);
            if (StringUtils.isBlank(destinationPath)) {
                logger.debug("Destination path empty for moving content - {}", sourceResource.getPath());
                workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.ERROR);
                workflowMetadataMap.put(TCBConstants.MESSAGE, "Invalid Traget Path : " + destinationPath);
                return;
            }

            Resource destinationResource = resolver.getResource(destinationPath);
            if (destinationResource != null) {
                deactivateResourceFromPublish(workflowMetadataMap, payload, session, sourceResource);
                Node destinationNode = destinationResource.adaptTo(Node.class);
                if (destinationNode.hasNode(sourceResource.getName())) {
                    logger.debug("File Already Exists in Destination Folder : - {}", destinationPath);
                    workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.ERROR);
                    workflowMetadataMap.put(TCBConstants.MESSAGE,
                            "File Already Exists in Destination Folder : " + destinationPath);
                    return;
                }
                // Check if the source is an asset or a page
                if (DamUtil.isAsset(sourceResource)) {
                    Asset sourceAsset = DamUtil.resolveToAsset(sourceResource);
                    Node sourceNode = sourceAsset.adaptTo(Node.class);
                    moveNode(sourceNode, destinationNode, session.getWorkspace());
                    workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.SUCCESS);
                    workflowMetadataMap.put(TCBConstants.MESSAGE,
                            "Asset Successfully Moved" + destinationResource.getPath() + sourceResource.getName());
                } else {
                    // Check if the destination path is valid for moving a page
                    resolver.move(sourceResource.getPath(), destinationResource.getPath());
                    workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.SUCCESS);
                    workflowMetadataMap.put(TCBConstants.MESSAGE,
                            "Page Successfully Moved" + destinationResource.getPath() + sourceResource.getName());
                }
                workflowMetadataMap.put(TCBConstants.DESTINATION_FILE_NAME,
                        destinationPath + TCBConstants.SLASH + sourceResource.getName());
                session.save();
                logger.debug("Moved Path - {}", destinationPath);
            } else {
                workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.ERROR);
                workflowMetadataMap.put(TCBConstants.MESSAGE,
                        "Invalid Target Path : " + destinationPath);
                logger.debug("Destination path is not valid");
            }
        } catch (RepositoryException | PersistenceException e) {
            workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.ERROR);
            workflowMetadataMap.put(TCBConstants.MESSAGE, "Exception : " + e.getMessage());
            logger.error("Failed to move content", e);
        }
    }

    public void deactivateResourceFromPublish(final MetaDataMap workflowMetadataMap, final String payload,
                                              final Session session, final Resource sourceResource) {
        ReplicationStatus replicationStatus = sourceResource.adaptTo(ReplicationStatus.class);
        if (replicationStatus != null && replicationStatus.isActivated()) {
            try {
                replicator.replicate(session, ReplicationActionType.DEACTIVATE, payload);
            } catch (ReplicationException e) {
                workflowMetadataMap.put(TCBConstants.STATUS, TCBConstants.ERROR);
                workflowMetadataMap.put(TCBConstants.MESSAGE, "Error in Deactivating " + payload);
                logger.error("Exception in Deactivate {} {}", e.getMessage(), e);
            }
        }
    }

    private void moveNode(final Node sourceNode, final Node destinationNode,
                          final Workspace workspace) throws RepositoryException {
        workspace.move(sourceNode.getPath(), destinationNode.getPath() + TCBConstants.SLASH + sourceNode.getName());
    }
}
