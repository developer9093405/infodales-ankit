package com.infodales.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.mail.Quota;
import java.util.ArrayList;

@Model(adaptables = Resource.class)
public class HomePageModel {
    private String message;
    private  ArrayList<String> myName;


    public  ArrayList<String> getMyName() {
        return myName;
    }


    @PostConstruct
    protected void init(){
        myName = new ArrayList<String>();
        message="Hii ankit ..............................";
        myName.add(1,"ankit");
        myName.add(2,"krishna");
        myName.add(3,"Ram");

    }

    public String getMessage() {
        return message;
    }

}
