package com.infodales.aem.core.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.*;


@Model(adaptables = SlingHttpServletRequest.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,adapters = RelatedMovie.class)
public class RelatedMovie {
    @Inject
    SlingHttpServletRequest request;

    @ScriptVariable
    private Page currentPage;
   // String path = "/content/idmedia-aem/us/en/home/movies/bahubali";
    @Inject
    private PageManager pageManager;
    @Inject
    private TagManager tagManager;
    @Inject
    private Resource resource;
    private String pageTitle;
    List<String>  pages;
    List<String> tagList;
    @Inject
    ResourceResolver resourceResolver;
    @PostConstruct
    protected void init() throws RepositoryException {
        Resource resource = resourceResolver.getResource(currentPage.getPath());

        PageManager pm = resourceResolver.adaptTo(PageManager.class);
        Page containingPage = pm.getContainingPage(resource);
        ValueMap pageProperties = containingPage.getProperties();
//        Resource resource=request.getResource();
//        Page currentPage = pageManager.getContainingPage(resource);
//        ValueMap pageProperties = currentPage.getProperties();
        pageTitle = (String) pageProperties.get("jcr:title");

        //Reading Genre tags
        String[] tags = (String[]) pageProperties.get("cq:generetags");
        TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        tagList = new ArrayList<>();
        if(tags!=null){
            for (String tag : tags) {
                Tag genreTag = tagManager.resolve(tag);
                if (genreTag != null) {
                    tagList.add(genreTag.getTitle());
                }
            }
        }
        pages =new ArrayList<>();
        pages.addAll(getPagesByTags(tags));


    }
    private List<String> getPagesByTags(String[] tags) throws RepositoryException {
        ResourceResolver resolver=request.getResourceResolver();
        QueryBuilder queryBuilder =resolver.adaptTo(QueryBuilder.class);
        Session session = resolver.adaptTo(Session.class);
        List<String> pages = new ArrayList<>();
        if (queryBuilder!=null && session!=null){
            Map<String,String> predicates = new HashMap<>();
            predicates.put("path","/content/idmedia-aem");
            predicates.put("type","cq:Page");
            predicates.put("1_property","jcr:content/cq:generetags");
            predicates.put("1_property.value","im-media:gener/action");
//
//            path=/content/idmedia-aem
//            type=cq:Page
//            1_property=jcr:content/cq:generetags
//            1_property.value=im-media:gener/action
//            predicates.put("type", "cq:Page"); // the type of resources to search
//            predicates.put("path", "/content/idmedia-aem"); // the root path where to search
//            predicates.put("fulltext", "Action");
//            predicates.put("group.p.or", "true"); // make sure the predicates are OR'ed
//            for (String tag : tags) {
//                predicates.put("fulltext", tag);
//            }
            Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
            SearchResult result = query.getResult();
            for (Hit hit : result.getHits()) {
                Resource resource = hit.getResource();
                Page page = resource.adaptTo(Page.class);
                if (page != null) {
                    pages.add( page.getPath());
                }
            }
        }
        return pages;
    }
    public List<String> getPages() {
        return pages;
    }
    public String getPageTitle() {
        return pageTitle;
    }

    public List<String> getTagList() {
        return tagList;
    }
}
