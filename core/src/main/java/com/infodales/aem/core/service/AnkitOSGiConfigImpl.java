package com.infodales.aem.core.service;


import com.infodales.aem.core.config.ankitOSGiConfig;
import com.infodales.aem.core.service.impl.AnkitOSGiConfigCall;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = AnkitOSGiConfigCall.class,immediate = true)
@Designate(ocd = ankitOSGiConfig.class )
public class AnkitOSGiConfigImpl implements AnkitOSGiConfigCall{



    private  static final Logger LOG = LoggerFactory.getLogger(AnkitOSGiConfigImpl.class);


    private ankitOSGiConfig Config;


    @Activate
    protected void activate(ankitOSGiConfig Config ){
        this.Config = Config;

    }


    @Override
    public String getHostName() {
        return Config.hostName();
    }

    @Override
    public int getPort() {
        return Config.portNo();
    }

    @Override
    public String getUniqueId() {
        return Config.hostUniqueID();
    }
}
