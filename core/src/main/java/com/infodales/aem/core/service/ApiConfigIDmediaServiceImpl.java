package com.infodales.aem.core.service;

import com.infodales.aem.core.config.apiConfigIDMedia;
import com.infodales.aem.core.service.impl.ApiConfigIDmediaService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = ApiConfigIDmediaService.class)
@Designate(ocd = apiConfigIDMedia.class)
public class ApiConfigIDmediaServiceImpl implements ApiConfigIDmediaService {
    private  static final Logger LOG = LoggerFactory.getLogger(ApiConfigIDmediaServiceImpl.class);
    @Reference
    private apiConfigIDMedia config;



    @Activate
    protected void activate(apiConfigIDMedia configIDMedia) {
        this.config = configIDMedia;
    }

    @Override
    public String getProtocol() {
        return config.protocol();
    }

    @Override
    public String getHostName() {
        return config.hostName();
    }

    @Override
    public String getRegistrationAPI() {
        return config.registrationAPI();
    }

    @Override
    public String getLoginAPI() {
        return config.loginAPI();
    }

    @Override
    public String getTokenValidationAPI() {
        return config.tokenValidationAPI();
    }

    @Override
    public String getUserProfileAPI() {
        return config.getUserProfileAPI();
    }

    @Override
    public String getForgetPasswordAPI() {
        return config.forgetPasswordAPI();
    }

    @Override
    public String getProfileUpdateAPI() {
        return config.profileUpdateAPI();
    }
}
